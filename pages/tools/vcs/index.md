# Version Control System

A version control system (or VCS) is a tool that records changes you make to your code and facilitates sharing this code with other developers.

The main VCS tool in use today is [Git](git). There are other VCSs out there, but I have not used them and I don't think they are worth your time. Almost all developers these days use Git.