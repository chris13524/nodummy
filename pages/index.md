---
title: No Dummy
---

# No Dummy

A [Chris Smith](https://chris.smith.xyz) project.

Hi, I'm Chris. I'm writing this website to share my knowledge. I believe that tutorials and documentation for languages is often overly verbose for my liking. As such, the readings here will be very dense and technical.

I don't believe in teaching readers the theory behind things and I also don't believe in only teaching "practical" skills. This guide serves to give readers a jump start in understanding how something works and best practices, nothing more. For those that wish to dive deeper into something, I will include various official resources and tools I use commonly.

This is the guide I wish I had when I was learning these skills.

Let's begin.

## Platforms

 - [web](platforms/web) - develop websites or applications for running in a web browser such as Firefox or Chrome

## Tools

 - [version control system](tools/vcs) - a tool that records changes to your code and facilitates sharing code between multiple developers