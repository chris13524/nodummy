---
title: Web
---

# Web

The "web" is a network of interlinked documents. Documents could be anything from research papers, to a business's website, to paid interactive applications.

## HTML

Documents are written using [HTML](html). HTML adds semantic meaning to plain text such as introducing paragraphs, adding emphasis, or putting things in a list. HTML does *not* specify how a document looks, just what it means.

## User agent (web browser)

A user can view and interact with these HTML documents by using a "user agent" (also known as a web browser (e.g. Firefox or Chrome)). A user agent is responsible for interacting with the web on behalf of a user. A user agent could also provide anything from assistive utilities to bookmarks or addons.

## CSS

[CSS](css) can be used to change how the user agent displays an HTML document. CSS does not add meaning to the document and will be ignored by user agents that e.g. work with blind people.

Most user agents have a default set of styles. E.g. emphasis should be rendered in italics.

## JavaScript

[JavaScript](js) is another optional feature that user agents usually provide. JavaScript is a general-purpose programming langauge that can be used on an HTML document to make more dynamic and interactive content.