---
title: HTML
---

# HTML

HTML is a document format that "marks up" text content by adding meaning.

HTML does not specify or care how the document looks, only what it means. [CSS](../css) can be used to customize how the document looks.

## Elements/Tags

Elements are the basic building blocks of HTML. Elements are created by using tags. Elements start using the `<tag>` tag and end with a `</tag>` tag.

Some elements don't have an end tag such as images (`<img>`). This is confusing.

## Boilerplate

On every HTML document that you write, you need a set of _boilerplate_ tags:

```html
<!DOCTYPE html>
<html>
  <body>
    
  </body>
</html>
```

All of the next tags go inside the `<body>` element.

## Paragraphs

Use the [`<p>`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/p) tag to create a paragraph element.

Here is an example of two paragraphs:

```html
<p>
  In publishing and graphic design, Lorem ipsum is a placeholder text
  commonly used to demonstrate the visual form of a document or a typeface 
  without relying on meaningful content. Lorem ipsum may be used before 
  final copy is available, but it may also be used to temporarily replace 
  copy in a process called greeking, which allows designers to consider 
  form without the meaning of the text influencing the design.
</p>
<p>
  Lorem ipsum is typically a corrupted version of De finibus bonorum et 
  malorum, a first-century BC text by the Roman statesman and philosopher 
  Cicero, with words altered, added, and removed to make it nonsensical, 
  improper Latin.
</p>
```

## Basic markup

Use the [`<em>`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/em) tag to emphasize content:

```html
<p>I <em>love</em> pizza.</p>
```

Use the [`<strong>`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/strong) tag to indicate content that is of strong importance:

```html
<p>I <strong>really</strong> need you to come look at this.</p>
```

## Lists

There are two types of list elements: the un-ordered list (`<ul>`) and the ordered list (`<ol>`). Inside each list you have list items (`<li>`).

So for instance, if we wanted to make a shopping list we would use an un-ordered list:

```html
<ul>
  <li>Milk</li>
  <li>Bread</li>
  <li>Eggs</li>
</ul>
```

This is a list of Milk, Bread, and Eggs.

Or if we wanted to write down a list of instructions we would use an ordered list:

```html
<ol>
  <li>Take out two slices of bread</li>
  <li>Put peanut butter on once slide</li>
  <li>Put jelly on the other slice</li>
  <li>Put the slices together into a sandwich</li>
</ol>
```

## Headers

Headers can be used to structure your document such that all elements afterwards are considered under that header.

So if you wanted to specify the title of your document, that would be a header level 1 and you would do that like this:

```html
<h1>Document Title</h1>
<p>In this awesome document, we are going to do some awesome stuff.</p>
```

If you have any subheadings, you can do the same using the other levels: `<h2>`, `<h3>`, `<h4>`, `<h5>`, and `<h6>`

## Attributes

Each tag can have various attributes added to it. Which attributes are available depend on which tag you are using.

## Links

Remember that the web is made up of inter-linked HTML documents? Links can be created by using an [`<a>`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/a) (**a**nchor) element in combination with its `href` attribute:

```html
My favorite search engine is <a href="https://duckduckgo.com/">DuckDuckGo</a>.
```

The content of the element (in this case "DuckDuckGo") is the name of the link.

The `href` attribute specifies what [URI](https://en.wikipedia.org/wiki/Uniform_Resource_Identifier) the link points to.

## Images

Images can be added using the `<img>` tag. Image tags need two attributes, `src` and `alt`.

```html
<img src="https://mysite.com/logo.png" alt="My Site">
```

Note that there is no ending tag (`</img>`). The image tag is the only tag that doesn't have an end tag.

The `src` attribute specifies the URL where the image file can be found.

The `alt` attribute specifies the text that could be substituted in place of the image. Remember since HTML is a text formatting system, you still need the text that the image goes in place of. For instance if the image was your logo, you would set the alt text to the name of your company. Or if the image has content that can't be easily represented with text (such as a photo or info graphic), the text would describe the contents of the image.

## Sections of content

HTML supports additional tags to describe sections of content. So for instance if your website has a navigation 

## Why semantic HTML?

You might think why do I need to bother with all these tags? Why does it matter what the document *means* when all I care about is how it *looks*?

Well, a few reasons:

 - It allows computers to better understand your content such as search engines or web scrapers.
 - It enables screen readers to interpret your content and read it to a blind person (it would be lame for a computer to be reading text saying "this text is very large" when it should've said "this is the title of the document").
 - It allows browser extensions to provide services to users. For instance if you put all your dates and times inside a [`<time>`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/time) tag, somebody could develop a browser extension that interprets these as an item that could be added to your calendar. Or if you put all your addresses inside an [`address`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/address) tag, your browser could prompt you to add it to your address book.

## More

At this point, I think you should have an decent understanding of how HTML works.

Here are some more tags to get you started:

 - `blockquote`
 - `header`

Here are some great resources:

 - MDN
